// workflow: we will schreenshot the page and save it to output directory
const puppeteer = require('puppeteer')

module.exports = async (url) => {
    try {
        // run browser not showing UI by set headless: false
        console.log('Opening URL:', url)
        const browser = await puppeteer.launch()
        // create a new page
        const page = await browser.newPage()
        // set viewport of the page or emulate
        // await page.setViewport({ width: 1366, height: 768 })
        await page.emulate(puppeteer.devices['iPhone 6'])
        // go to website
        await page.goto(url, { waitUntil: 'networkidle0' })
        // take screenshot
        const outputfile = process.cwd() + '/output/new_image.png'
        await page.screenshot({
            path: outputfile, fullPage: true
        })
        console.log('Location of the screenshot:', outputfile)
        // close page
        await page.close()
        // close browser
        await browser.close()
    } catch (error) {
        console.log('Error: ', error.message)
    }
    process.exit()
}

